package com.brainardphotography.clockpi.weather

fun Double.toFahrenheit() = (this - 273.15) * 1.8
