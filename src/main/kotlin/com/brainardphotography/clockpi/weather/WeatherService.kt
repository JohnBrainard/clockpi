package com.brainardphotography.clockpi.weather

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
	@GET("/data/2.5/weather")
	fun getCurrentForecast(
			@Query("id") cityId: String,
			@Query("appid") appId: String): Call<Forecast>
}