package com.brainardphotography.clockpi.weather

import com.fasterxml.jackson.annotation.JsonProperty

class Forecast() {
	@JsonProperty("coord")
	lateinit var coordinates: ForecastCoordinates

	@JsonProperty("weather")
	lateinit var weather: Array<ForecastWeather>

	@JsonProperty("base")
	lateinit var base: String

	@JsonProperty("main")
	lateinit var main: ForecastMain

	@JsonProperty("wind")
	lateinit var wind: ForecastWind

	@JsonProperty("clouds")
	lateinit var clouds: ForecastClouds

	@JsonProperty("sys")
	lateinit var internal: ForecastInternal

	@JsonProperty("dt")
	var dt: Int = 0

	@JsonProperty("id")
	var id: Int = 0

	@JsonProperty("name")
	lateinit var name: String

	@JsonProperty("cod")
	var cod: Int = 0
}

class ForecastCoordinates {
	@JsonProperty("lon")
	var longitude: Double = 0.0

	@JsonProperty("lat")
	var latitutde: Double = 0.0
}

class ForecastWeather {
	var id: Int = -1
	lateinit var main: String
	lateinit var description: String
	lateinit var icon: String
}

class ForecastMain {
	@JsonProperty("temp")
	var temperature: Double = 0.0

	@JsonProperty("pressure")
	var pressure: Double = 0.0

	@JsonProperty("humidity")
	var humidity: Int = 0

	@JsonProperty("temp_min")
	var temperatureMin: Double = 0.0

	@JsonProperty("temp_max")
	var temperatureMax: Double = 0.0

	@JsonProperty("sea_level")
	var seaLevel: Double = 0.0

	@JsonProperty("grnd_level")
	var groundLevel: Double = 0.0
}

class ForecastWind {
	@JsonProperty("speed")
	var speed: Double = 0.0

	@JsonProperty("deg")
	var degrees: Double = 0.0
}

class ForecastClouds {
	@JsonProperty("all")
	var all: Int = 0
}

class ForecastInternal {
	@JsonProperty("message")
	var message: Double = 0.0

	@JsonProperty("country")
	lateinit var country: String

	@JsonProperty("sunrise")
	var sunrise: Int = 0

	@JsonProperty("sunset")
	var sunset: Int = 0
}