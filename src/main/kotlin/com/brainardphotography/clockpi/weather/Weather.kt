package com.brainardphotography.clockpi.weather

import com.brainardphotography.clockpi.ClockApplication
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

object Weather {
	private val service: WeatherService

	init {
		val interceptor = HttpLoggingInterceptor();
		interceptor.level = HttpLoggingInterceptor.Level.BODY;
		val client = OkHttpClient.Builder().addInterceptor(interceptor).build();

		val retrofit = Retrofit.Builder()
				.baseUrl("http://api.openweathermap.org")
				.client(client)
				.addConverterFactory(JacksonConverterFactory.create())
				.build()

		service = retrofit.create(WeatherService::class.java)
	}

	fun getCurrentForecast(city: String): Forecast {
		val call = service.getCurrentForecast(city, ClockApplication.weatherApiKey)

		val response = call.execute()
		if (response.isSuccessful)
			return response.body()

		throw RuntimeException("Unable to make weather call: ${response.errorBody()}")
	}
}