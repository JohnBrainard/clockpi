package com.brainardphotography.clockpi

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Parent
import javafx.scene.Scene
import javafx.scene.text.Font
import javafx.stage.Stage
import java.util.*

class ClockApplication : Application() {
	companion object {
		val WIDTH=480.0
		val HEIGHT=800.0

		var imagePath:String? = null
		var weatherApiKey:String

		init {
			val properties = Properties().apply {
				javaClass.getResource("/application.properties").openStream().use { stream ->
					load(stream)
				}
			}

			weatherApiKey = properties.getProperty("weather.apikey")
		}
	}

	override fun start(stage: Stage) {
		stage.isFullScreen = parameters.named["windowed"]?.toBoolean() ?: true
		imagePath = parameters.named["images"] ?: null

		val root: Parent = FXMLLoader.load(javaClass.getResource("/clock.fxml"))
		stage.scene = Scene(root, WIDTH, HEIGHT)

		Font.loadFont(javaClass.getResource("/LLPIXEL3.ttf").toExternalForm(), 11.0)
		Font.loadFont(javaClass.getResource("/Roboto-Thin.ttf").toExternalForm(), 11.0)
		Font.loadFont(javaClass.getResource("/Roboto-Regular.ttf").toExternalForm(), 11.0)
		Font.loadFont(javaClass.getResource("/DS-DIGI.TTF").toExternalForm(), 11.0)

		stage.show()
	}

}

fun main(args: Array<String>) {
	Application.launch(ClockApplication::class.java, *args)
}