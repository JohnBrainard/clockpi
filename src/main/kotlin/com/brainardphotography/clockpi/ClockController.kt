package com.brainardphotography.clockpi

import javafx.animation.Animation
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.image.ImageView
import javafx.scene.text.Text
import javafx.util.Duration
import java.net.URL
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class ClockController : Initializable {
	@FXML
	lateinit var timeText: Text

	@FXML
	lateinit var secondText: Text

	@FXML
	lateinit var dateText: Text

	@FXML
	lateinit var imageView: ImageView

	@FXML
	lateinit var animationController: AnimationController

	private val timeFormatter = DateTimeFormatter.ofPattern("h:mm")
	private val secondFormatter = DateTimeFormatter.ofPattern("ss")
	private val dateFormatter = DateTimeFormatter.ofPattern("eeee, MMM d")

	override fun initialize(location: URL?, resources: ResourceBundle?) {
		updateTime()

		val timeline = Timeline(KeyFrame(Duration.millis(30.0), EventHandler {
			updateTime()
		}))
		timeline.cycleCount = Animation.INDEFINITE
		timeline.play()

	}

	fun updateTime() {
		val now = LocalDateTime.now()

		timeText.text = now.format(timeFormatter)
		secondText.text = now.format(secondFormatter)
		dateText.text = now.format(dateFormatter)
	}

}