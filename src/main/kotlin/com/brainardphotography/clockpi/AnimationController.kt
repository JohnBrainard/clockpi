package com.brainardphotography.clockpi

import javafx.animation.KeyFrame
import javafx.animation.KeyValue
import javafx.animation.Timeline
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.util.Duration
import java.io.File
import java.net.URL
import java.util.*

class AnimationController : Initializable {
	@FXML
	lateinit var imageView: ImageView

	companion object {
		val FRAME_LENGTH = 150.0 //ms
		val FINAL_FRAME_LENGTH = 3000.0 //ms
	}

	override fun initialize(location: URL?, resources: ResourceBundle?) {
		val imageTimeline = Timeline()
		val slices = loadImages()

		slices.forEachIndexed { i, slice ->
			imageTimeline.keyFrames.add(KeyFrame(Duration(i * FRAME_LENGTH), KeyValue(imageView.imageProperty(), Image(slice))))
		}

		imageTimeline.keyFrames.add(KeyFrame(Duration(slices.size * FRAME_LENGTH + FINAL_FRAME_LENGTH)))

		imageTimeline.onFinished = EventHandler {
			imageTimeline.playFromStart()
		}

		imageTimeline.play()
	}

	private fun loadImages(): List<String> {
		if (ClockApplication.imagePath != null) {
			val imageFolder = File(ClockApplication.imagePath)
			return imageFolder.listFiles().map { it.toURI().toString() }.sorted()
		}

		return Collections.emptyList()
	}
}