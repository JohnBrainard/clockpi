package com.brainardphotography.clockpi.weather

import spock.lang.Specification

class WeatherTest extends Specification {
	def "getCurrentForecast() returns the weather for the selected city"() {
		given:
		String cityId = "2172797"

		when:
		Forecast forecast = Weather.INSTANCE.getCurrentForecast(cityId)

		then:
		forecast != null
	}
}
